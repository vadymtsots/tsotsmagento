<?php

namespace MusicStore\Album\Block;

use Magento\Framework\View\Element\Template;
use MusicStore\Album\Api\AlbumRepositoryInterface;
use MusicStore\Album\Api\Data\AlbumInterface;
use MusicStore\Album\Model\AlbumRepositor;

class ViewAlbum extends Template
{

    protected $_eventPrefix = "album";

    protected $_eventObject = "album";

    /**
     * @var AlbumRepositor
     */
    private $albumRepository;

    /**
     * Authors constructor.
     * @param Template\Context $context
     * @param AlbumRepositoryInterface $albumRepository
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        AlbumRepositoryInterface $albumRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->albumRepository = $albumRepository;
    }

    public function getAlbum() : ?AlbumInterface
    {
        $id = $this->getRequest()->getParam("id");
        return $this->albumRepository->getById((int)$id);
    }
}
