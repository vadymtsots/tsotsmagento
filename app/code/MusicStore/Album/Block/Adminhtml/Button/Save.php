<?php

namespace MusicStore\Album\Block\Adminhtml\Button;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class Save implements ButtonProviderInterface
{
    public function getButtonData()
    {
        return [
            'label' => __('Save'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => [
                    'buttonAdapter' => [
                        'actions' => [
                            [
                                'targetName' => 'album_form.album_form',
                                'actionName' => 'save',
                                'params' => [
                                    false

                                ]
                            ]
                        ]
                    ]
                ]
            ],
        ];
    }

}
