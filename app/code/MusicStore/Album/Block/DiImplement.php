<?php

namespace MusicStore\Album\Block;


use MusicStore\Album\Api\Data\AlbumInterface;
use MusicStore\Album\Model\Album;
use Magento\Framework\View\Element\Template;
use MusicStore\Album\Model\ResourceModel\Album as ResourceAlbum;


class DiImplement extends Template
{
    private $album;

    private $albumResource;

    public function __construct(
        Template\Context $context,
        AlbumInterface $album,
        ResourceAlbum $albumResource,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->album = $album;
        $this->albumResource = $albumResource;
    }

    public function getAlbum()
    {
        return $this->album->load(2);
    }
}

