<?php

namespace MusicStore\Album\Block;

use Magento\Framework\View\Element\Template;
use MusicStore\Album\Model\AlbumFactory;
use MusicStore\Album\Model\ResourceModel\Album\CollectionFactory as AlbumCollectionFactory;
use MusicStore\Album\Model\ResourceModel\AlbumFactory as AlbumResourceFactory;
use MusicStore\Album\Model\AlbumRepositor;
use MusicStore\Album\Api\Data\AlbumInterface;
use MusicStore\Album\Api\AlbumRepositoryInterface;


class Albums extends Template
{
    private $albumRepositor;
    private $albumFactory;
    private $albumResourceFactory;
    private $albumCollectionFactory;
    public function __construct(
        Template\Context $context,
        AlbumRepositoryInterface $albumRepositor,
        AlbumFactory $albumFactory,
        AlbumResourceFactory $albumResourceFactory,
        AlbumCollectionFactory $albumCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        //$this->album = $album;
        $this->albumFactory = $albumFactory;
        $this->albumResourceFactory = $albumResourceFactory;
        $this->albumCollectionFactory = $albumCollectionFactory;
        $this->albumRepositor = $albumRepositor;
    }

    public function getAlbum()
    {
        /*$album = $this->albumFactory->create();
        $this->albumResourceFactory->create()->load($album, 1);
        return $album; */


        return $this->albumRepositor->getById(6);
    }

    public function getAlbums()
    {
        $collection = $this->albumCollectionFactory->create();


        return $collection;
    }

    public function getAlbumFromRepo()
    {
        return $this->albumRepositor->getById(1);
    }
}
