<?php

declare(strict_types=1);
namespace MusicStore\Album\Model;

use Magento\Framework\Model\AbstractModel;
use MusicStore\Album\Api\Data\AlbumInterface;
use MusicStore\Album\Model\ResourceModel\Album as cdAlbumResource;

class Album extends AbstractModel implements AlbumInterface
{
    protected $_eventPrefix = "album";

    protected $_eventObject = "album";


    public function _construct()
    {
        $this->_init(cdAlbumResource::class); //initializing resource model
    }

    /**
     * @return null|int
     */
    public function getAlbumId(): ?int
    {
        return (int)$this->getData(self::ALBUM_ID);
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->getData(self::NAME);
    }

    /**
     * @return string|null
     */
    public function getArtist(): ?string
    {
        return $this->getData(self::ARTIST);
    }

    /**
     * @return int|null
     */
    public function getReleaseYear(): ?int
    {
        return (int)$this->getData(self::RELEASE_YEAR);
    }

    /**
     * @return int|null
     */
    public function getNumberTracks(): ?int
    {
        return (int)$this->getData(self::NUMBER_TRACKS);
    }

    /**
     * @return string|null
     */
    public function getCreationTime() : ?string
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * @return string|null
     */
    public function getUpdateTime() : ?string
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * @param int $id
     * @return self
     */
    public function setAlbumId(int $id) : AlbumInterface
    {
        return $this->setData(self::ALBUM_ID, $id);
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name) : AlbumInterface
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * @param string $artist
     * @return $this|AlbumInterface
     */
    public function setArtist(string $artist) : AlbumInterface
    {
        return $this->setData(self::ARTIST, $artist);
    }

    public function setReleaseYear(int $release_year) : AlbumInterface
    {
        return $this->setData(self::RELEASE_YEAR, $release_year);
    }

    public function setNumberTracks(int $number_tracks) : AlbumInterface
    {
        return $this->setData(self::NUMBER_TRACKS, $number_tracks);
    }

    public function setCreationTime(string $creation_time) : AlbumInterface
    {
        return $this->setData(self::CREATION_TIME, $creation_time);
    }

    public function setUpdateTime(string $update_time) : AlbumInterface
    {
        return $this->setData(self::UPDATE_TIME, $update_time);
    }
}
