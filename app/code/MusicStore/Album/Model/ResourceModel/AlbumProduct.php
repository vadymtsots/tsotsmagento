<?php

namespace MusicStore\Album\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class AlbumProduct extends AbstractDb
{
    public function _construct()
    {
        $this->_init("album_product", "id");
    }
}
