<?php


namespace MusicStore\Album\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Album extends AbstractDb
{
    public function _construct()
    {
        $this->_init("album", "album_id");
    }
}
