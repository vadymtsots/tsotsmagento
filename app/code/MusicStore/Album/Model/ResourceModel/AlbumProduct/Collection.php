<?php


namespace MusicStore\Album\Model\ResourceModel\AlbumProduct;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use MusicStore\Album\Model\AlbumProduct;
use MusicStore\Album\Model\ResourceModel\AlbumProduct as AlbumProductResource;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(AlbumProduct::class, AlbumProductResource::class);
    }
}
