<?php

namespace MusicStore\Album\Model\ResourceModel\Album;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use MusicStore\Album\Model\Album;
use MusicStore\Album\Model\ResourceModel\Album as AlbumResource;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(Album::class, AlbumResource::class);
    }
}
