<?php

declare(strict_types=1);
namespace MusicStore\Album\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use MusicStore\Album\Api\AlbumRepositoryInterface;
use MusicStore\Album\Api\Data\AlbumInterface;
use MusicStore\Album\Model\ResourceModel\AlbumFactory as AlbumResourceFactory;

class AlbumRepositor implements AlbumRepositoryInterface
{
    /**
     * @var \MusicStore\Album\Model\AlbumFactory
     */
    private $albumFactory;
    /**
     * @var AlbumResourceFactory
     */
    private $albumResourceFactory;

    /**
     * AlbumRepositor constructor.
     * @param AlbumFactory $albumFactory
     * @param AlbumResourceFactory $albumResourceFactory
     */

    public function __construct(

        AlbumFactory $albumFactory,
        AlbumResourceFactory $albumResourceFactory
    ) {
        $this->albumFactory = $albumFactory;
        $this->albumResourceFactory = $albumResourceFactory;
    }

    public function getById(int $id) : AlbumInterface
    {

        /* try {
            $album = $this->albumFactory->create();
            $this->albumResourceFactory->create()->load($album, $id);
            if (!$album->getId()) {
                throw new NoSuchEntityException(_("There is no album"));
            }
            return $album;

        } */
        $album = $this->albumFactory->create();
        $this->albumResourceFactory->create()->load($album, $id);
        return $album;
    }

    /**
     * @param AlbumInterface $album
     * @return void
     */

    public function save(AlbumInterface $album) : void
    {
        $this->albumResourceFactory->create()->save($album);
    }

    /**
     * @param AlbumInterface $album
     * @return void
     */

    public function delete(AlbumInterface $album) : void
    {
        $this->albumResourceFactory->create()->delete($album);
    }

    public function deleteById(int $id) : void
    {
        $album = $this->getById($id);
        $this->delete($album);
    }
}
