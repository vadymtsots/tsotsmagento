<?php

declare(strict_types=1);

namespace MusicStore\Album\Model;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Ui\DataProvider\Modifier\PoolInterface;
use Magento\Ui\DataProvider\ModifierPoolDataProvider;
use MusicStore\Album\Api\AlbumRepositoryInterface;
use MusicStore\Album\Model\ResourceModel\Album\CollectionFactory;

class DataProvider extends ModifierPoolDataProvider
{
    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var AlbumFactory
     */
    protected $albumFactory;
    /**
     * @var AlbumRepositoryInterface
     */
    protected $albumRepository;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param array $meta
     * @param array $data
     * @param null $pool
     * @param RequestInterface $request
     * @param AlbumRepositoryInterface $albumRepository
     * @param AlbumFactory $albumFactory
     * @param CollectionFactory $collectionFactory
     */


    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = [],
        $pool = null,
        RequestInterface $request,
        AlbumRepositoryInterface $albumRepository,
        AlbumFactory $albumFactory,
        CollectionFactory $collectionFactory
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
        $this->request = $request;
        $this->albumRepository = $albumRepository;
        $this->albumFactory = $albumFactory;
        $this->collectionFactory = $collectionFactory;
        $this->collection = $collectionFactory->create();
    }

    public function getData()
    {
        $albumId = $this->request->getParam($this->requestFieldName);
        try {
            $album = $this->albumRepository->getById((int)$albumId);
        } catch (NoSuchEntityException $exception) {
            $album = $this->albumFactory->create();
        }
        $this->loadedData[$album->getId()] = $album->getData();
        return $this->loadedData;
    }
}
