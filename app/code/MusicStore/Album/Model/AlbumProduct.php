<?php

declare(strict_types=1);
namespace MusicStore\Album\Model;

use Magento\Framework\Model\AbstractModel;
use MusicStore\Album\Api\Data\AlbumProductInterface;
use MusicStore\Album\Model\ResourceModel\Album as cdAlbumResource;

class AlbumProduct extends AbstractModel implements AlbumProductInterface
{
    protected $_eventPrefix = "album";

    protected $_eventObject = "album";

    public function _construct()
    {
        $this->_init(cdAlbumResource::class); //initializing resource model
    }

    public function getAlbumId() : ?int
    {
        return (int)$this->getData(self::ALBUM_ID);
    }

    public function getProductId() : ?int
    {
        return (int)$this->getData(self::PRODUCT_ID);
    }

    public function setAlbumId(int $album_id): AlbumProductInterface
    {
        return $this->setData(self::ALBUM_ID, $album_id);
    }

    public function setProductId(int $product_id): AlbumProductInterface
    {
        return $this->setData(self::PRODUCT_ID, $product_id);
    }
}
