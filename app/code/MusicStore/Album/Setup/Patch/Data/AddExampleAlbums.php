<?php

namespace MusicStore\Album\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use MusicStore\Album\Api\AlbumRepositoryInterface;
use MusicStore\Album\Model\AlbumFactory;

class AddExampleAlbums implements DataPatchInterface
{
    private $albumRepository;

    private $albumFactory;

    public function __construct(
        albumRepositoryInterface $albumRepository,
        albumFactory $albumFactory
    ) {
        $this->albumRepository = $albumRepository;
        $this->albumFactory = $albumFactory;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

    public function apply()
    {
        $firstAlbum = $this->albumFactory->create();
        $firstAlbum->setName("South of Heaven");
        $firstAlbum->setArtist("Slayer");

        $secondAlbum = $this->albumFactory->create();
        $secondAlbum->setName("Digimortal");
        $secondAlbum->setArtist("Fear Factory");
    }
}
