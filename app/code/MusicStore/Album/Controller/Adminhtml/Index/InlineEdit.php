<?php

namespace MusicStore\Album\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use MusicStore\Album\Api\AlbumRepositoryInterface as AlbumRepository;
use MusicStore\Album\Model\AlbumFactory;

class InlineEdit extends Action implements HttpPostActionInterface
{
    const ADMIN_RESOURCE = 'MusicStore_Album::album';

    protected $dataProcessor;

    protected $albumRepository;

    protected $jsonFactory;

    protected $albumFactory;

    public function __construct(
        Context $context,
        AlbumRepository $albumRepository,
        JsonFactory $jsonFactory,
        AlbumFactory $albumFactory
    ) {
        parent::__construct($context);
        $this->albumRepository = $albumRepository;
        $this->jsonFactory = $jsonFactory;
        $this->albumFactory = $albumFactory;
    }

    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];
        if (($this->getRequest()->getParam('isAjax'))) {
            $postItems = $this->getRequest()->getParam('items', []);
            if (empty($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $albumId) {
                    $albumData = $this->albumRepository->getById($albumId);
                    try {
//                        $user = $this->userFactory->create();
                        $albumData->setData($postItems[$albumId]);
                        $this->albumRepository->save($albumData);
                    } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
                        $messages[] = "[Error:]  {$exception->getMessage()}";
                        $error = true;
                    }
                }
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }
}
