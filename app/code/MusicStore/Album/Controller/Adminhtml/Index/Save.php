<?php

namespace MusicStore\Album\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use MusicStore\Album\Api\AlbumRepositoryInterface;
use MusicStore\Album\Model\AlbumFactory;

class Save extends Action implements HttpGetActionInterface, HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'MusicStore_Album::album';

    /**
     * @var AlbumRepositoryInterface
     */
    private $albumRepository;

    /**
     * @var AlbumFactory
     */
    private $albumFactory;

    /**
     * @var RedirectFactory
     */
    private $redirectFactory;

    /**
     * Save constructor.
     * @param Context $context
     * @param AlbumRepositoryInterface $albumRepository
     * @param AlbumFactory $albumFactory
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Context $context,
        AlbumRepositoryInterface $albumRepository,
        AlbumFactory $albumFactory,
        RedirectFactory $redirectFactory
    ) {
        parent::__construct($context);
        $this->albumRepository = $albumRepository;
        $this->albumFactory = $albumFactory;
        $this->redirectFactory = $redirectFactory;
    }

    public function execute()
    {
        $albumData = $this->getRequest()->getParams();

        $album = $this->albumFactory->create();

        $album->setData($albumData);

        if (!$album->getData("album_id")) {
            $album->unsetData("album_id");
        }

        $this->albumRepository->save($album);

        return $this->redirectFactory->create()->setPath("*/*/index");
    }
}
