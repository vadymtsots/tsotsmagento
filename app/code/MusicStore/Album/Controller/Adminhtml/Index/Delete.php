<?php

namespace MusicStore\Album\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use MusicStore\Album\Api\AlbumRepositoryInterface;

class Delete extends Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'MusicStore_Album::delete';

    /**
     * @var AlbumRepositoryInterface
     */
    private $albumRepository;

    /**
     * Delete constructor.
     * @param Context $context
     * @param AlbumRepositoryInterface $albumRepository
     */
    public function __construct(
        Context $context,
        AlbumRepositoryInterface $albumRepository
    ) {
        $this->albumRepository = $albumRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $album_id = $this->getRequest()->getParam("album_id");
        if (!$album_id) {
            $this->messageManager->addErrorMessage("No album specified");
        }

        try {
            $this->albumRepository->deleteById($album_id);
            $this->messageManager->addSuccessMessage("Album removed");
        } catch (NoSuchEntityException $exception) {
            $this->messageManager->addErrorMessage("Cannot find album to delete");
        }

        $redirect = $this->resultRedirectFactory->create();

        return $redirect->setPath("*/*/index");
    }
}
