<?php

namespace MusicStore\Album\Api\Data;

interface AlbumInterface
{
    const ALBUM_ID = "album_id";
    const NAME = "name";
    const ARTIST = "artist";
    const RELEASE_YEAR = "release_year";
    const NUMBER_TRACKS = "number_tracks";
    const CREATION_TIME = "creation_time";
    const UPDATE_TIME = "update_time";

    /**
     * @return int|null
     */
    public function getAlbumId() : ?int;

    /**
     * @return string|null
     */
    public function getName() : ?string;

    /**
     * @return string|null
     */
    public function getArtist() : ?string;

    /**
     * @return int|null
     */
    public function getReleaseYear() : ?int;

    /**
     * @return int|null
     */
    public function getNumberTracks() : ?int;

    /**
     * @return string|null
     */
    public function getCreationTime() : ?string;

    /**
     * @return string|null
     */
    public function getUpdateTime() : ?string;

    /**
     * @param int $id
     * @return $this|null
     */
    public function setAlbumId(int $id) : AlbumInterface;

    /**
     * @param string $name
     * @return $this|null
     */
    public function setName(string $name) : AlbumInterface;

    /**
     * @param string $artist
     * @return AlbumInterface
     */

    public function setArtist(string $artist) : AlbumInterface;

    /**
     * @param int $release_year
     * @return AlbumInterface
     */

    public function setReleaseYear(int $release_year) : AlbumInterface;

    /**
     * @param int $number_tracks
     * @return AlbumInterface
     */

    public function setNumberTracks(int $number_tracks) : AlbumInterface;

    /**
     * @param string $creation_time
     * @return AlbumInterface
     */

    public function setCreationTime(string $creation_time) : AlbumInterface;

    /**
     * @param string $update_time
     * @return AlbumInterface
     */

    public function setUpdateTime(string $update_time) : AlbumInterface;






}
