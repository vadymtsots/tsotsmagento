<?php

namespace MusicStore\Album\Api\Data;

interface AlbumProductInterface
{
    const ID = "id";
    const ALBUM_ID = "album_id";
    const PRODUCT_ID = "product_id";



    /**
     * @return int|null
     */
    public function getAlbumId() : ?int;

    /**
     * @return int|null
     */
    public function getProductId() : ?int;


    /**
     * @param int $album_id
     * @return AlbumProductInterface|null
     */
    public function setAlbumId(int $album_id) : ?AlbumProductInterface;

    /**
     * @param int $product_id
     * @return AlbumProductInterface|null
     */
    public function setProductId(int $product_id) : ?AlbumProductInterface;
}
