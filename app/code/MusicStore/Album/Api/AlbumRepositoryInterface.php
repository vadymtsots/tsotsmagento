<?php
declare(strict_types=1);
namespace MusicStore\Album\Api;

use MusicStore\Album\Api\Data\AlbumInterface;

interface AlbumRepositoryInterface
{
    /**
     * @param int $id
     * @return AlbumInterface
     */
    public function getById(int $id) : AlbumInterface;

    /**
     * @param AlbumInterface $album
     * @return void
     */
    public function save(AlbumInterface $album) : void;

    /**
     * @param AlbumInterface $album
     * @return void
     */
    public function delete(AlbumInterface $album) : void;

    /**
     * @param int $id
     * @return void
     */
    public function deleteById(int $id) : void;
}
