<?php

declare(strict_types=1);

namespace MusicStore\Album\Api;

use MusicStore\Album\Api\Data\AlbumProductInterface;

interface AlbumProductRepositoryInterface
{
    /**
     * @param int $id
     * @return AlbumProductInterface
     */
    public function getById(int $id): AlbumProductInterface;

    /**
     * @param int $id
     * @return AlbumProductInterface
     */

    public function getByProductId(int $id): AlbumProductInterface;

    /**
     * @param AlbumProductInterface $album
     * @return void
     */
    public function save(AlbumProductInterface $album): void;

    /**
     * @param AlbumProductInterface $album
     * @return void
     */

    public function delete(AlbumProductInterface $album): void;

    /**
     * @param int $id
     * @return void
     */

    public function deleteById(int $id): void;
}
