<?php
namespace MusicStore\Import\Console;

use MusicStore\Import\Api\Service\ImportInterface;
use MusicStore\Import\Service\GenericCSVImport;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SomeCommand
 */
class Import extends Command
{
    private $importers;
    public function __construct(
        $name = null,
        array $importers = []
    ) {
        parent::__construct($name);
        $this->importers = $importers;
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('musicstore:import');
        $this->setDescription('MusicStore DI configuration demo');

        parent::configure();
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach ($this->importers as $importer) {
            $result = $importer->execute();
        }

        if ($result) {
            $output->writeln('<info>Imported!</info>');
        } else {
            $output->writeln('<info>Import failed!</info>');
        }

//        $output->writeln('<info>Success Message.</info>');
//        $output->writeln('<error>An error encountered.</error>');
//        $output->writeln('<comment>Some Comment.</comment>');
    }
}
