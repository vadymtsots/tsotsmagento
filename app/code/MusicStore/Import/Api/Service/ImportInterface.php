<?php

declare(strict_types=1);

namespace MusicStore\Import\Api\Service;

interface ImportInterface
{
    public function execute();
}
