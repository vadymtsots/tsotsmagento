<?php

declare(strict_types=1);

namespace MusicStore\AlbumExtend\ViewModel;

use MusicStore\Album\Helper\Data;

class AlbumViewModel implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    /**
     * @var Data
     */
    private $albumHelper;

    /**
     * AlbumViewModel constructor.
     * @param Data $albumHelper
     */
    public function __construct(
        Data $albumHelper
    ) {
        $this->albumHelper = $albumHelper;
    }

    /**
     * @return bool
     */
    public function isAlbumModuleEnabled() : bool
    {
        return $this->albumHelper->isEnabled();
    }
}
