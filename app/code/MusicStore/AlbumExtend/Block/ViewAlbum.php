<?php

namespace MusicStore\AlbumExtend\Block;

use Magento\Framework\Exception\NoSuchEntityException;
use MusicStore\Album\Api\Data\AlbumInterface;
use MusicStore\Album\Block\ViewAlbum as OriginalViewAlbum;

class ViewAlbum extends OriginalViewAlbum
{
    public function getAlbum(): ?AlbumInterface
    {
        try {
            return parent::getAlbum();
        } catch (NoSuchEntityException $exception) {
            echo __("Album NOT FOUND");
        }
        return null;
    }
}
