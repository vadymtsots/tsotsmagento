<?php

namespace MusicStore\AlbumExtend\Observer;

use Magento\Framework\Event\ObserverInterface;
use MusicStore\Album\Api\Data\AlbumInterface;

class AlbumLoadAfter implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var AlbumInterface $album */
        $album = $observer->getAlbum();
        $album->setArtist($album->getArtist() . "???");
    }
}
