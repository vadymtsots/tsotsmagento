<?php

namespace Elogic\Author\Block;

use Elogic\Author\Model\Author;
use Magento\Framework\View\Element\Template;

class Authors extends Template
{
    private $author;
    public function __construct(
        Template\Context $context,
        Author $author,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->author = $author;
    }

    public function getAuthor()
    {
        return $this->author->load(1);
    }
}
