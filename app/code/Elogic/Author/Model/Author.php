<?php

namespace Elogic\Author\Model;

use Magento\Framework\Model\AbstractModel;
use Elogic\Author\Model\ResourceModel\Author as AuthorResource;

class Author extends AbstractModel
{
    public function _construct()
    {
        $this->_init(AuthorResource::class); //initializing resource model
    }
}
