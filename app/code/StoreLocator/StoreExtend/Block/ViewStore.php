<?php

namespace StoreLocator\StoreExtend\Block;

use Magento\Framework\Exception\NoSuchEntityException;

use StoreLocator\Store\Api\Data\StoreInterface;

use StoreLocator\Store\Block\Stores as OriginalStore;

class ViewStore extends OriginalStore
{
    public function getStore(): ?StoreInterface
    {
        try {
            return parent::getStore();
        } catch (NoSuchEntityException $exception) {
            echo __("Store NOT FOUND");
        }
        return null;
    }
}
