<?php

declare(strict_types=1);

namespace StoreLocator\StoreExtend\ViewModel;

use StoreLocator\Store\Helper\Data;

class StoreViewModel implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    /**
     * @var Data
     */
    private $storeHelper;

    /**
     * AuthorViewModel constructor.
     * @param Data $storeHelper
     */
    public function __construct(
        Data $storeHelper
    ) {
        $this->storeHelper = $storeHelper;
    }

    /**
     * @return bool
     */
    public function isStoreModuleEnabled() : bool
    {
        return $this->storeHelper->isEnabled();
    }
}
