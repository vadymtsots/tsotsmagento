<?php

declare(strict_types=1);

namespace StoreLocator\Store\Service;

class Importer
{
    private $importers;

    public function __construct(
        array $importers = []
    ) {
        $this->importers = $importers;
    }
}
