<?php

declare(strict_types=1);

namespace StoreLocator\Store\Service;

use StoreLocator\Store\Api\Service\ImportInterface;


class GenericImport implements ImportInterface
{
    private $fileName;



    public function __construct(
        string $fileName = null
    ) {
        $this->fileName = $fileName;

    }

    public function execute()
    {
         if (!$this->fileName) {
             return false;
         }

         echo "\n";
         echo $this->fileName;
         echo "\n";
         //TODO process file;

         return true;


    }
}
