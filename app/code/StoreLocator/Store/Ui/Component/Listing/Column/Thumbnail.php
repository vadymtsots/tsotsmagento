<?php
/** * GiaPhuGroup Co., Ltd. * * NOTICE OF LICENSE * * This source file is subject to the GiaPhuGroup.com license that is * available through the world-wide-web at this URL: * https://www.giaphugroup.com/LICENSE.txt * * DISCLAIMER * * Do not edit or add to this file if you wish to upgrade this extension to newer * version in the future. * * @category PHPCuong * @package PHPCuong_BannerSlider * @copyright Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/) * @license https://www.giaphugroup.com/LICENSE.txt */
namespace StoreLocator\Store\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Store\Model\StoreManagerInterface;
use StoreLocator\Store\Model\Store;

class Thumbnail extends \Magento\Ui\Component\Listing\Columns\Column
{
    /** * Url path */
    const URL_PATH_EDIT = 'store/index/edit';

    /** * @var Store */
    protected $store;

    private $urlBuilder;

    private $storeManager;

    /** * Image constructor. *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param Store $store
     * @param array $components
     * @param array $data */

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        Store $store,
        StoreManagerInterface $storeManager,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->urlBuilder = $urlBuilder;
        $this->store = $store;
        $this->storeManager = $storeManager;
    }
    /** Prepare Data Source
     * @param array $dataSource
     * @return array
     */

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $url = '';
                if ($item[$fieldName] != '') {
                    $url = $this->storeManager->getStore()->getBaseUrl() . 'pub/media/storelocator/store/images/' . $item[$fieldName];
                    $store = new \Magento\Framework\DataObject($item);
                    $item[$fieldName . '_src'] = $url;
                    $item[$fieldName . '_orig_src'] = $url;
                    $item[$fieldName . '_link'] = $this->urlBuilder->getUrl(self::URL_PATH_EDIT, ['store1_id' => $store['store1_id']]);
                    $item[$fieldName . '_alt'] = $store['name'];
                }
            }

        }
        return $dataSource;
    }
}
