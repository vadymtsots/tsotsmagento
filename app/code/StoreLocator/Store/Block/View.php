<?php

namespace StoreLocator\Store\Block;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\View\Element\Template;
use StoreLocator\Store\Api\StoreRepositoryInterface;
use StoreLocator\Store\Model\ResourceModel\StoreFactory as StoreResourceFactory;
use StoreLocator\Store\Model\StoreFactory;
use StoreLocator\Store\Model\ResourceModel\Store\CollectionFactory as StoreCollectionFactory;

class View extends Template
{
    /**
     * @var StoreFactory
     */
    private $storeFactory;

    /**
     * @var StoreResourceFactory
     */
    private $storeResourceFactory;

    /**
     * @var StoreRepositoryInterface
     */
    private $storeRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteria;

    /**
     * @var StoreCollectionFactory
     */
    private $storeCollectionFactory;

    /**
     * View constructor.
     * @param Template\Context $context
     * @param StoreRepositoryInterface $storeRepository
     * @param StoreFactory $storeFactory
     * @param StoreResourceFactory $storeResourceFactory
     * @param SearchCriteriaBuilder $searchCriteria
     * @param StoreCollectionFactory $storeCollectionFactory
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        StoreRepositoryInterface $storeRepository,
        StoreFactory $storeFactory,
        StoreResourceFactory $storeResourceFactory,
        SearchCriteriaBuilder $searchCriteria,
        StoreCollectionFactory $storeCollectionFactory,
        array $data = []
    ) {
        $this->storeRepository = $storeRepository;
        $this->storeFactory = $storeFactory;
        $this->storeResourceFactory = $storeResourceFactory;
        $this->searchCriteria = $searchCriteria;
        $this->storeCollectionFactory = $storeCollectionFactory;
        parent::__construct($context, $data);
    }

    public function getStore()
    {
        return $this->storeRepository->getById(3);
    }

    public function getStoreCollection()
    {
        {
            return $this->storeRepository->getList(
                $this->searchCriteria->create()
            )->getItems();
        }

    }

    public function getStores()
    {
        $collection = $this->storeCollectionFactory->create();


        return $collection;
    }
}
