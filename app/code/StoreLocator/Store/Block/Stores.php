<?php

namespace StoreLocator\Store\Block;

use Magento\Framework\View\Element\Template;
use StoreLocator\Store\Api\StoreRepositoryInterface;
use StoreLocator\Store\Model\ResourceModel\StoreFactory as StoreResourceFactory;
use StoreLocator\Store\Model\StoreFactory;

class Stores extends Template
{
    /**
     * @var StoreFactory
     */
    private $storeFactory;

    /**
     * @var StoreResourceFactory
     */
    private $storeResourceFactory;

    /**
     * @var StoreRepositoryInterface
     */
    private $storeRepository;

    public function __construct(
        Template\Context $context,
        StoreRepositoryInterface $storeRepository,
        StoreFactory $storeFactory,
        StoreResourceFactory $storeResourceFactory,
        array $data = []
    ) {
        $this->storeFactory = $storeFactory;
        $this->storeResourceFactory = $storeResourceFactory;
        $this->storeRepository = $storeRepository;
        parent::__construct($context, $data);
    }

    public function getStoreById()
    {
        $id = $this->getRequest()->getParam("store1_id");
        return $this->storeRepository->getById($id);

    }
}
