<?php

namespace StoreLocator\Store\Console;

use Magento\Customer\Model\Customer;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\File\Csv;
use StoreLocator\Store\Api\StoreRepositoryInterface;
use StoreLocator\Store\Model\ResourceModel\Store\Collection as StoreCollection;
use StoreLocator\Store\Model\StoreFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Import extends Command
{

    /**
     * @var Csv
     */
    private $csv;
    /**
     * @var Customer
     */
    private $customer;
    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var AdapterInterface
     */
    private $connection;
    /**
     * @var StoreCollection
     */
    private $collection;

    private $importers;

    private $storeRepository;

    private $storeFactory;

    const TABLE_NAME = 'store1';

    protected $resource;

    /**
     * Import constructor.
     * @param CollectionFactory $collectionFactory
     * @param Csv $csv
     * @param Customer $customer
     * @param DirectoryList $directoryList
     * @param ResourceConnection $resource
     * @param StoreRepositoryInterface $storeRepository
     * @param StoreFactory $storeFactory
     * @param string|null $name
     * @param array $importers
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        Csv $csv,
        Customer $customer,
        DirectoryList $directoryList,
        ResourceConnection $resource,
        StoreRepositoryInterface $storeRepository,
        StoreFactory $storeFactory,
        string $name = null,
        array $importers = []
    ) {
        parent::__construct($name);
        $this->importers = $importers;
        $this->collection = $collectionFactory;
        $this->csv = $csv;
        $this->connection = $resource->getConnection();
        $this->resource = $resource;
        $this->customer = $customer;
        $this->directoryList = $directoryList;
        $this->storeRepository = $storeRepository;
        $this->storeFactory = $storeFactory;
    }

    /**
     * Configures the current command.
     * Your command will be :  bin/magento elogic:import --importData=1 (or --exportData=1)
     */
    protected function configure()
    {
        $this->setName('storelocator:import');
        $this->setDescription('Storelocator CSV Import');
        $this->setDefinition([
            new InputOption('importData', null, InputOption::VALUE_NONE, 'import the csv file'),
//            new InputOption('exportData',null, InputOption::VALUE_OPTIONAL, 'export to a csv')
        ]);
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            if (!is_null($input->getOption('importData'))) {
                $this->importCsvFile();
            }

//            if (!is_null($input->getOption('exportData'))) {
//                $this->exportCsvFile();
//            }
        } catch (\Exception $e) {
            $output->writeln('<comment>An exception was thrown during area code setting:</comment>');
            $output->writeln($e->getMessage());
        }

//        foreach ($this->importers as $importer) {
//            $result = $importer->execute();
//
//            if ($result) {
//                $output->writeln('<info>Imported</info>');
//            } else {
//                $output->writeln('<error>Failed</error>');
//
//            }
//        }

//        $output->writeln('<info>Success Message.</info>');
//        $output->writeln('<error>An error encountered.</error>');
//        $output->writeln('<comment>Some Comment.</comment>');
    }

    private function importCsvFile()
    {
        $filename = '1location.csv';
        $file = $this->directoryList->getPath('var') . '/' . $filename;
        $csvData = fopen($file, 'r');
        $data = [];
        $i=0;
        while ($row = fgetcsv($csvData)) {
            $data[$i]['name']  = trim($row[1]);
            $data[$i]['address']  = trim($row[5]);
            $i++;
        }

        $tableName = $this->resource->getTableName(self::TABLE_NAME);
        $this->connection->insertMultiple($tableName, $data);
    }

    /*   /**
        * @return StoreCollection
        * @throws \Magento\Framework\Exception\LocalizedException
        */
  /*  private function getCollection()
    {
        $this->collection = $this->storeCollectionFactory->create();
        $this->collection->addAttributeToSelect(
            [
                'store1_id',
                'name',
                'address',
                'description',
                'image',
                'working_hours',
                'url_key'
            ]
        );
        return $this->collection;
    } */

  /*  /**
     * @return array
     */
  /*  private function getHeaders()
    {
        $data = [
            'Customer_Id',
            'Firstname',
            'Lastname',
            'Email'
        ];
        return $data;
    }  */
}
