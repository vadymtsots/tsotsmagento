<?php

namespace StoreLocator\Store\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use StoreLocator\Store\Api\StoreRepositoryInterface;

class Delete extends Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'StoreLocator_Store::delete';

    /**
     * @var StoreRepositoryInterface
     */
    private $storeRepository;

    /**
     * Delete constructor.
     * @param Context $context
     * @param StoreRepositoryInterface $storeRepository
     */
    public function __construct(
        Context $context,
        StoreRepositoryInterface $storeRepository
    ) {
        $this->storeRepository = $storeRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $store_id = $this->getRequest()->getParam("store1_id");
        if (!$store_id) {
            $this->messageManager->addErrorMessage("No store specified");
        }

        try {
            $this->storeRepository->deleteById($store_id);
            $this->messageManager->addSuccessMessage("Store removed");
        } catch (NoSuchEntityException $exception) {
            $this->messageManager->addErrorMessage("Cannot find store to delete");
        }

        $redirect = $this->resultRedirectFactory->create();

        return $redirect->setPath("*/*/index");
    }
}
