<?php

namespace StoreLocator\Store\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use StoreLocator\Store\Api\StoreRepositoryInterface;
use StoreLocator\Store\Model\StoreFactory;

class InlineEdit extends Action implements HttpPostActionInterface
{
    const ADMIN_RESOURCE = 'StoreLocator_Store::store';

    protected $dataProcessor;

    protected $storeRepository;

    protected $jsonFactory;

    protected $storeFactory;

    public function __construct(
        Context $context,
        StoreRepositoryInterface $storeRepository,
        JsonFactory $jsonFactory,
        StoreFactory $storeFactory
    ) {
        parent::__construct($context);
        $this->storeRepository = $storeRepository;
        $this->jsonFactory = $jsonFactory;
        $this->storeFactory = $storeFactory;
    }

    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];
        if (($this->getRequest()->getParam('isAjax'))) {
            $postItems = $this->getRequest()->getParam('items', []);
            if (empty($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $storeId) {
                    $storeData = $this->storeRepository->getById($storeId);
                    try {
//                        $user = $this->userFactory->create();
                        $storeData->setData($postItems[$storeId]);
                        $this->storeRepository->save($storeData);
                    } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
                        $messages[] = "[Error:]  {$exception->getMessage()}";
                        $error = true;
                    }
                }
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }
}
