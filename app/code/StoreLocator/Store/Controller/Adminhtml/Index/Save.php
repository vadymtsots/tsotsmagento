<?php

namespace StoreLocator\Store\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use StoreLocator\Store\Api\StoreRepositoryInterface;
use StoreLocator\Store\Model\StoreFactory;

class Save extends Action implements HttpGetActionInterface, HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'StoreLocator_Store::store';

    /**
     * @var StoreRepositoryInterface
     */
    private $storeRepository;

    /**
     * @var StoreFactory
     */
    private $storeFactory;

    /**
     * @var RedirectFactory
     */
    private $redirectFactory;

    /**
     * Save constructor.
     * @param Context $context
     * @param StoreRepositoryInterface $storeRepository
     * @param StoreFactory $storeFactory
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Context $context,
        StoreRepositoryInterface $storeRepository,
        StoreFactory $storeFactory,
        RedirectFactory $redirectFactory
    ) {
        parent::__construct($context);
        $this->storeRepository = $storeRepository;
        $this->storeFactory = $storeFactory;
        $this->redirectFactory = $redirectFactory;
    }

    public function execute()
    {
        $storeData = $this->getRequest()->getParams();

        $store = $this->storeFactory->create();

        $store->setData($storeData);

        if (!$store->getData("store1_id")) {
            $store->unsetData("store1_id");
        }

        $this->storeRepository->save($store);

        return $this->redirectFactory->create()->setPath("*/*/index");
    }
}
