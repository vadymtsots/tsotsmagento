<?php

namespace StoreLocator\Store\Controller;

use StoreLocator\Store\Model\ResourceModel\Store\CollectionFactory as StoreCollectionFactory;

class Router implements \Magento\Framework\App\RouterInterface
{
    /**
     * @var \Magento\Framework\App\ActionFactory
     */
    protected $actionFactory;

    /**
     * @var StoreCollectionFactory
     */
    protected $collection;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        \Magento\Framework\App\ActionFactory $actionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        StoreCollectionFactory $collection
    ) {
        $this->actionFactory = $actionFactory;
        $this->collection = $collection;
        $this->storeManager = $storeManager;
    }

    /**
     * Get store identifier
     *
     * @return  int
     */
    public function getStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }

    /**
     *
     * @param \Magento\Framework\App\RequestInterface $request
     * @return bool
     */
    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        $identifier = trim($request->getPathInfo(), '/');
        $id = explode('/', $identifier, 3);

        $route = 'store';

        if ((isset($id[0]) && ($id[0] != $route)) || !isset($id[0])) {
            return false;
        }

        $identifier = substr_replace($request->getPathInfo(), '', 0, strlen("/" . $route . "/"));
        $identifier = str_replace('.html', '', $identifier);
        $identifier = str_replace('.htm', '', $identifier);

        if ($identifier == '') {
            $request->setModuleName('store')
                ->setControllerName('storelocator')
                ->setActionName('view');
            return true;
        }
        $identifier = trim($identifier, '/');
        $stores = $this->collection->create()
            ->addFieldToFilter('url_key', $identifier)
            ->getFirstItem();
        if ($stores->getId()) {
            $request->setModuleName('store')
                ->setControllerName('storelocator')
                ->setActionName('index')
                ->setParams(['store1_id'=>$stores->getId()]);
            $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $identifier);
            return $this->actionFactory->create(
                'Magento\Framework\App\Action\Forward',
                ['request' => $request]
            );
        }
        return true;
    }
}
