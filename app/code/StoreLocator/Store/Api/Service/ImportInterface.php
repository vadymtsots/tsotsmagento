<?php

namespace StoreLocator\Store\Api\Service;

interface ImportInterface
{
    public function execute();
}
