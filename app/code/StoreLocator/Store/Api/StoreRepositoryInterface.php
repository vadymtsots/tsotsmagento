<?php

namespace StoreLocator\Store\Api;

use Magento\Framework\Api\SearchResultsInterface;
use StoreLocator\Store\Api\Data\StoreInterface;
use StoreLocator\Store\Model\Store;
use Magento\Framework\Api\SearchCriteriaInterface;

interface StoreRepositoryInterface
{
    /**
     * @param int $id
     * @return StoreInterface
     */
    public function getById(int $id) : StoreInterface;

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param StoreInterface $store
     * @return void
     */
    public function save(StoreInterface $store) : void;

    /**
     * @param StoreInterface $store
     * @return void
     */
    public function delete(StoreInterface $store) : void;

    /**
     * @param int $id
     * @return void
     */
    public function deleteById(int $id) : void;
}
