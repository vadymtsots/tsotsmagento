<?php

namespace StoreLocator\Store\Api\Data;

interface StoreInterface
{
    const STORE_ID = "store1_id";
    const NAME = "name";
    const DESCRIPTION = "description";
    const IMAGE = "image";
    const ADDRESS = "address";
    const WORKING_HOURS = "working_hours";
    const LONGITUDE = "longitude";
    const LATITUDE = "latitude";
    const CREATION_TIME = "creation_time";
    const UPDATE_TIME = "update_time";
    const URL_KEY = "url_key";

    /**
     * @return int|null
     */
    public function getStoreId() : ?int;

    /**
     * @return null|string
     */
    public function getName() : ?string;

    /**
     * @return null|string
     */
    public function getDescription() : ?string;

    /**
     * @return null|string
     */
    public function getImage() : ?string;

    /**
     * @return null|string
     */
    public function getAddress() : ?string;

    /**
     * @return null|string
     */
    public function getWorkingHours() : ?string;

    /**
     * @return null|string
     */
    public function getLongitude() : ?string;

    /**
     * @return null|string
     */
    public function getLatitude() : ?string;

    /**
     * @return null|string
     */
    public function getCreationTime() : ?string;

    /**
     * @return null|string
     */
    public function getUpdateTime() : ?string;

    /**
     * @return null|string
     */
    public function getUrlKey() : ?string;

    /**
     * @param int $id
     * @return StoreInterface
     */
    public function setStoreId(int $id) : StoreInterface;

    /**
     * @param string $name
     * @return StoreInterface
     */
    public function setName(string $name) : StoreInterface;

    /**
     * @param string $description
     * @return StoreInterface
     */
    public function setDescription(string $description) : StoreInterface;

    /**
     * @param string $image
     * @return StoreInterface
     */
    public function setImage(string $image) : StoreInterface;

    /**
     * @param string $address
     * @return StoreInterface
     */
    public function setAddress(string $address) : StoreInterface;

    /**
     * @param string $working_hours
     * @return StoreInterface
     */
    public function setWorkingHours(string $working_hours) : StoreInterface;

    /**
     * @param string $longitude
     * @return StoreInterface
     */
    public function setLongitude(string $longitude) : StoreInterface;

    /**
     * @param string $latitude
     * @return StoreInterface
     */
    public function setLatitude(string $latitude) : StoreInterface;

    /**
     * @param string $creation_time
     * @return StoreInterface
     */

    public function setCreationTime(string $creation_time) : StoreInterface;

    /**
     * @param string $update_time
     * @return StoreInterface
     */

    public function setUpdateTime(string $update_time) : StoreInterface;

    /**
     * @param string $url_key
     * @return StoreInterface
     */
    public function setUrlKey(string $url_key) : StoreInterface;
}
