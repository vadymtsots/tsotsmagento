<?php

namespace StoreLocator\Store\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface StoreSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get items
     * @return \StoreLocator\Store\Api\Data\StoreInterface[]
     */
    public function getItems();
    /**
     * Set items
     * @param \StoreLocator\Store\Api\Data\StoreInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
