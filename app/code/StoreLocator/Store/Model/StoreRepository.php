<?php

declare(strict_types=1);

namespace StoreLocator\Store\Model;

use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use StoreLocator\Store\Api\StoreRepositoryInterface;
use StoreLocator\Store\Api\Data\StoreInterface;
use StoreLocator\Store\Model\ResourceModel\StoreFactory as StoreResourceFactory;
use StoreLocator\Store\Model\StoreFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use StoreLocator\Store\Model\ResourceModel\Store\CollectionFactory as StoreCollectionFactory;

class StoreRepository implements StoreRepositoryInterface
{
    /**
     * @var StoreResourceFactory
     */
    private $storeResourceFactory;

    /**
     * @var StoreFactory
     */
    private $storeFactory;

    /**
     * @var StoreCollectionFactory
     */
    private $storeCollectionFactory;

    /**
     * @var SearchCriteriaInterface
     */
    private $searchCriteria;

    /**
     * @var \StoreLocator\Store\Api\Data\StoreSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * StoreRepository constructor.
     * @param \StoreLocator\Store\Model\StoreFactory $storeFactory
     * @param StoreResourceFactory $storeResourceFactory
     * @param StoreCollectionFactory $storeCollectionFactory
     * @param SearchCriteriaInterface $searchCriteria
     */

    public function __construct(
        StoreFactory $storeFactory,
        StoreResourceFactory $storeResourceFactory,
        StoreCollectionFactory $storeCollectionFactory,
        SearchCriteriaInterface $searchCriteria,
        \StoreLocator\Store\Api\Data\StoreSearchResultsInterfaceFactory $searchResultsFactory
    )
    {
        $this->storeFactory = $storeFactory;
        $this->storeResourceFactory = $storeResourceFactory;
        $this->storeCollectionFactory = $storeCollectionFactory;
        $this->searchCriteria = $searchCriteria;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * @param int $id
     * @return StoreInterface
     */
    public function getById(int $id) : StoreInterface
    {
        $store = $this->storeFactory->create();
        $this->storeResourceFactory->create()->load($store, $id);

        return $store;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return  SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $storeCollection = $this->storeCollectionFactory->create();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $storeCollection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $searchResults->setTotalCount($storeCollection->getSize());
        $sortOrdersData = $searchCriteria->getSortOrders();
        if ($sortOrdersData) {
            foreach ($sortOrdersData as $sortOrder) {
                $storeCollection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $storeCollection->setCurPage($searchCriteria->getCurrentPage());
        $storeCollection->setPageSize($searchCriteria->getPageSize());
        $searchResults->setItems($storeCollection->getData());
        return $searchResults;
    }

    /**
     * @param StoreInterface $store
     * @return void
     */
    public function save(StoreInterface $store) : void
    {
        $this->storeResourceFactory->create()->save($store);
    }

    /**
     * @param StoreInterface $store
     * @return void
     */
    public function delete(StoreInterface $store) : void
    {
        $this->storeResourceFactory->create()->delete($store);
    }

    public function deleteById(int $id) : void
    {
        $store = $this->getById($id);
        $this->delete($store);
    }
}
