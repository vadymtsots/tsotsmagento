<?php

namespace StoreLocator\Store\Model\ResourceModel\Store;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use StoreLocator\Store\Model\ResourceModel\Store as StoreResource;
use StoreLocator\Store\Model\Store;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(Store::class, StoreResource::class);
    }
}
