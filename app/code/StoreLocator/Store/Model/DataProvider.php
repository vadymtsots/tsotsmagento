<?php

namespace StoreLocator\Store\Model;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Ui\DataProvider\Modifier\PoolInterface;
use Magento\Ui\DataProvider\ModifierPoolDataProvider;
use StoreLocator\Store\Api\StoreRepositoryInterface;
use StoreLocator\Store\Model\ResourceModel\Store\CollectionFactory;

class DataProvider extends ModifierPoolDataProvider
{
    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var StoreFactory
     */
    protected $storeFactory;
    /**
     * @var StoreRepositoryInterface
     */
    protected $storeRepository;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param array $meta
     * @param array $data
     * @param null $pool
     * @param RequestInterface $request
     * @param StoreRepositoryInterface $storeRepository
     * @param StoreFactory $storeFactory
     * @param CollectionFactory $collectionFactory
     */


    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = [],
        $pool = null,
        RequestInterface $request,
        StoreRepositoryInterface $storeRepository,
        StoreFactory $storeFactory,
        CollectionFactory $collectionFactory
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
        $this->request = $request;
        $this->storeRepository = $storeRepository;
        $this->storeFactory = $storeFactory;
        $this->collectionFactory = $collectionFactory;
        $this->collection = $collectionFactory->create();
    }

    public function getData()
    {
        $storeId = $this->request->getParam($this->requestFieldName);
        try {
            $store = $this->storeRepository->getById((int)$storeId);
        } catch (NoSuchEntityException $exception) {
            $store = $this->storeFactory->create();
        }
        $this->loadedData[$store->getId()] = $store->getData();
        return $this->loadedData;
    }
}
