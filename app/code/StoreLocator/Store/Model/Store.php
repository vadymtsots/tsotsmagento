<?php

namespace StoreLocator\Store\Model;

use Magento\Framework\Model\AbstractModel;
use StoreLocator\Store\Api\Data\StoreInterface;
use StoreLocator\Store\Model\ResourceModel\Store as StoreResource;

class Store extends AbstractModel implements StoreInterface
{
    public function _construct()
    {
        $this->_init(StoreResource::class);
    }

    /**
     * @return int|null
     */
    public function getStoreId() : ?int
    {
        return (int)$this->getData(self::STORE_ID);
    }

    /**
     * @return null|string
     */
    public function getName() : ?string
    {
        return $this->getData(self::NAME);
    }

    /**
     * @return null|string
     */
    public function getDescription() : ?string
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @return null|string
     */
    public function getImage() : ?string
    {
        return $this->getData(self::IMAGE);
    }

    /**
     * @return null|string
     */
    public function getAddress() : ?string
    {
        return $this->getData(self::ADDRESS);
    }

    /**
     * @return null|string
     */
    public function getWorkingHours() : ?string
    {
        return $this->getData(self::WORKING_HOURS);
    }

    /**
     * @return null|string
     */
    public function getLongitude() : ?string
    {
        return $this->getData(self::LONGITUDE);
    }

    /**
     * @return null|string
     */
    public function getLatitude() : ?string
    {
        return $this->getData(self::LATITUDE);
    }

    /**
     * @return null|string
     */
    public function getCreationTime() : ?string
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * @return null|string
     */
    public function getUpdateTime() : ?string
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * @return null|string
     */
    public function getUrlKey() : ?string
    {
        return $this->getData(self::URL_KEY);
    }


    /**
     * @param int $id
     * @return StoreInterface
     */
    public function setStoreId(int $id) : StoreInterface
    {
        return $this->setData(self::STORE_ID, $id);
    }

    /**
     * @param string $name
     * @return StoreInterface
     */
    public function setName(string $name) : StoreInterface
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * @param string $description
     * @return StoreInterface
     */
    public function setDescription(string $description) : StoreInterface
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @param string $image
     * @return StoreInterface
     */
    public function setImage(string $image) : StoreInterface
    {
        return $this->setData(self::IMAGE, $image);
    }

    /**
     * @param string $address
     * @return StoreInterface
     */
    public function setAddress(string $address) : StoreInterface
    {
        return $this->setData(self::ADDRESS, $address);
    }

    /**
     * @param string $working_hours
     * @return StoreInterface
     */
    public function setWorkingHours(string $working_hours) : StoreInterface
    {
        return $this->setData(self::WORKING_HOURS, $working_hours);
    }

    /**
     * @param string $longitude
     * @return StoreInterface
     */
    public function setLongitude(string $longitude) : StoreInterface
    {
        return $this->setData(self::LONGITUDE, $longitude);
    }

    /**
     * @param string $latitude
     * @return StoreInterface
     */
    public function setLatitude(string $latitude) : StoreInterface
    {
        return $this->setData(self::LATITUDE, $latitude);
    }

    /**
     * @param string $creation_time
     * @return StoreInterface
     */

    public function setCreationTime(string $creation_time) : StoreInterface
    {
        return $this->setData(self::CREATION_TIME, $creation_time);
    }

    /**
     * @param string $update_time
     * @return StoreInterface
     */

    public function setUpdateTime(string $update_time) : StoreInterface
    {
        return $this->setData(self::UPDATE_TIME, $update_time);
    }

    /**
     * @param string $url_key
     * @return StoreInterface
     */
    public function setUrlKey(string $url_key) : StoreInterface
    {
        return $this->setData(self::URL_KEY, $url_key);

    }


}
